/* 
 * File:   main.c
 * Author: vagrant
 *
 * Created on April 30, 2015, 1:56 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/*
 * 
 */

static int timer = 0;
void timerPlusOne(int sig){
  system("clear");
  printf("%d\n",timer++);
  if(timer %2 == 0){
    printf("/\n");
  }
  else{
    printf("\\ \n");
  }
  alarm(1);
}

int main(int argc, char** argv) {
  signal(SIGALRM,timerPlusOne);
  alarm(1);
  for(;;){
    pause();    
  }
  return (EXIT_SUCCESS);
}

