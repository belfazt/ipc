/* 
 * File:   main.c
 * Author: vagrant
 *
 * Created on April 29, 2015, 9:24 PM
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define	SERVER	"/tmp/server"

int main(int argc, char** argv) {
  int reader;
  int writer;
  ssize_t n;
  char name[128], buff[128];
  pid_t pid;


  pid = getpid();
  sprintf(name, "/tmp/%ld", (long) pid);
  if ((mkfifo(name, 0644) < 0))
    printf("can't create %s\n", name);

  sprintf(buff, "%ld\n", (long) pid);

  writer = open(SERVER, O_WRONLY, 0);
  write(writer, buff, strlen(buff));

  reader = open(name, O_RDONLY, 0);
  read(reader, buff, 128);
  printf("%s\n", buff);

  close(reader);
  unlink(name);

  return (0);
}
