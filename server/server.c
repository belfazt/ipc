/* 
 * File:   main.c
 * Author: vagrant
 *
 * Created on April 29, 2015, 9:51 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>

#define	SERVER	"/tmp/server"

void sig_int(int sig) {
  printf("Recv %d\n", sig);
  int fd = open(SERVER, O_RDWR);
  write(fd, strdup("TERMINATOR"), strlen("TERMINATOR") + 1);
  close(fd);
}

int main(int argc, char** argv) {
  int reader; 
  int writer;
  char buff[128], name[128];
  pid_t pid;
  ssize_t n;

  if ((mkfifo(SERVER, 0644) < 0)) {
    printf("can't create %s\n", SERVER);
  }

  signal(SIGINT, sig_int);
  reader = open(SERVER, O_RDWR, 0);


  while ((n = read(reader, buff, 128)) > 0) {
    buff[n] = '\0'; /* null terminate pathname */
    if (!strcmp(buff, "TERMINATOR")) {
      printf("I came from the future, to change my present, I'll be back\n");
      break;
    }
    pid = atol(buff);
    sprintf(name, "/tmp/%ld", (long) pid);
    if ((writer = open(name, O_WRONLY, 0)) < 0) {
      printf("cannot open: %s\n", name);
      continue;
    }
    sprintf(buff, "Hello %ld", (long) pid);
    write(writer, buff, sizeof (buff));
    close(writer);
  }
  unlink(SERVER);
  return (0);
}